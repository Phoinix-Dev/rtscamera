# Phoinix RTS Camera - Unreal Engine Plugin
#### An easy way to add a RTS camera to any existing project
*Add a RTS camera to your game easy and fast using this Plugin. It's easy to integrate with existing projects, and it can be adapted to any project. The camera can be moved using the mouse based on the screen borders or keys, it supports smooth or stepped zoom and rotation*

#### [Download Plugin UE 5.4](Builds/Phoinix RTS Camera Plugin 5.4-1.1.3.zip)
#### [Download Plugin UE 5.3](Builds/Phoinix RTS Camera Plugin 5.3-1.1.3.zip)
#### [Download Plugin UE 5.2](Builds/Phoinix RTS Camera Plugin 5.2-1.1.1.zip)
**[Legacy]**
#### [Download Plugin UE 5.1](Builds/Phoinix RTS Camera Plugin 5.1-1.0.zip)
#### [Download Plugin UE 5.0](Builds/Phoinix RTS Camera Plugin 5.0-1.0.zip)
#### [Download Plugin UE 4.27](Builds/Phoinix RTS Camera Plugin 4.27-1.0.zip)

![Demo](Resources/RTS Camera Demo.mov)

***
Core Features:
- Mouse based movement
- Key based movement
- Smooth camera rotation
- Smooth camera zoom

***
[![Discord](Resources/discord-logo-blue.png)](https://discord.gg/S7NuQa8Aeb)
***
## Documentation

### 1. Installation
You can download the plugin using the download links above. The installation can be done in three steps.

1. Copy the complete downloaded RTSCameraPlugin to the “YourProject/Plugins” folder. If there is no such Plugins folder in your Project, simple create one.

2. Start your Project. You can now use the RTSPawn. Create a new Blueprint inherited from the RTSPawn and select it as pawn in your GameMode. Makes sure to replace the GameMode with the newly created one.

3. Legacy only: You can create your very own Input-mapping. There is a preset available under the RTSCameraPlugin/Resources/Inputmapping.ini, you can import it under Projectsettings→Input→import.

### 2. Input
The Input Action and the Input Mapping Context can be found at "Plugin/InputMapping". 
It is setup by default with BP_RTSCamera. 

**[Legacy]**
As mentioned in the installation part, there is a preset for the input mapping. The mapping consists of the following bindings. 
- Axis Mouse Movement
- Axis Key Movement
- Camera Rotation
- Zoom

There are corresponding Blueprint Nodes available.


![This is an image](Resources/Input-Nodes.png)

### 3. Options
The RTS camera is heavily customizable in Blueprint. The most important option is to choose whether the camera should follow the landscape. This means, it changes the height based on mountains etc. It makes sense to create a custom channel specifically for the landscape and select it in the RTS Pawn. The height adjustment itself is a simple checkbox in the pawn. Furthermore, the behavior of zooming and rotating can be selected, from very smooth to step based.

Here is a summary of the options.

**Options** 

| Name | Details |
| ---      | ---      |
| MovementZoneInPercent |  Defines the Area in % of the viewport in which the Mouse Movement is activated |
| IgnoreBoundaries | Ignores the boundaries, allows movement with the mouse off the viewport |
| Movement Multiplier  | Multiply the movement speed |
| Movement Smoothness | Smaller values make the movement more smooth |
| HeightAdjustment | Whether the camera should be adjusted to the terrain or maintain the same height |
| TraceChannel | The channel used for the height adjustment  |
| TraceFromCamera | Whether the camera should be used for the height adjusted instead of the screen center   |
| Threshold | The required height difference required to adjust the camera |
| ClampToStartHeight | Whether the value should always be returned to the start height again |
| HeightAdjustmentSmoothness | Smaller values make the movement more smooth |
| RotationMultiplier | Multiplier for the rotation Speed |
| RotationAxis | The name of the Input Axis for the rotation |
| RotationSmoothness | Smaller values make the rotation more smooth |
| ZoomAxis | The name of the Input Axis for the Zoom |
| ZoomMultiplier | Multiplier for the zoom Speed |
| ZoomSmoothness | Smaller values make the zoom more smooth |
| MinSpringArm | The minimum length of the springarm |
| MaxSpringArm | The maximum length of the springarm |
| ZoomMultiplier | Multiplier for the zoom Speed |
| ZoomMultiplier | Multiplier for the zoom Speed |

___
